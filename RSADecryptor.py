from mathtools import *
import time

class RSADecryptor:
    
    # On construit le decrypteur avec un couple (p, q), si e n'est pas fourni, il sera alors généré comme le premier nombre tel que p,q < e < phi avec pgcd(n, e) == 1
    def __init__(self, p, q, e=0):
        self.p = p
        self.q = q
        self.generateN()
        self.generatePhi()
        if (e==0):
            self.generateE()
        else:
            self.e = e
        self.generateD()

    def generateN(self):
        self.n = self.q * self.p

    def generatePhi(self):
        self.phi = (self.p - 1) * (self.q - 1)

    def generateE(self):
        e = max(self.p+1, self.q+1)
        while(gcd(self.n, e) != 1):
            e = e+1
        self.e = e

    # On génère d tel que p,q < e < phi avec e*d % phi == 1
    def generateD(self):
        print("Generating d...")
        startTime = time.time()
        self.d = max(self.p, self.q)
        while((self.e * self.d) % self.phi != 1): 
            self.d = self.d+1
        print("Generated d = " + str(self.d) + " in", time.time() - startTime, "seconds")
        return

    def publicKey(self):
        return "(" + str(self.e) + ", " + str(self.n) + ")"

    def printRecap(self):
        print("p : " + str(self.p))
        print("q : " + str(self.q))
        print("n : " + str(self.n))
        print("phi : " + str(self.phi))
        print("e : " + str(self.e))
        print("d : " + str(self.d))

    # On décrypte un nombre avec resultat = nombre^d % n
    def numberDecryption(self, number):
        return pow(number, self.d) % self.n

    # On parse un array en construisant un autre parallèlement en decryptant chaque nombre de l'array original
    def numberArrayDecryption(self, array):
        # To test
        decryption = []
        for i in range(0, len(array)):
            decryption.append(self.numberDecryption(array[i]))
        return decryption
    
    # Decryption d'un array de nombres correspondant à l'encryptage d'un texte dans RSACryptor::textEncryption() en un string selon la position des caractères dans la table ASCII
    def textDecryption(self, arrayedText):
        decryption = ""
        for i in range (0, len(arrayedText)):
            decryption += chr(self.numberDecryption(arrayedText[i]))
        return decryption