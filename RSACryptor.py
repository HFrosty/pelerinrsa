from mathtools import *

class RSACryptor:

    # On construit un crypteur avec un couple (e, n)
    def __init__(self, e, n):
        self.e = e
        self.n = n

    # Crypte un nombre tel que resultat = nombre^e % n
    def numberEncryption(self, number):
        return pow(number, self.e) % self.n

    # On parse un array en construisant un autre parallèlement en cryptant chaque nombre de l'array original
    def numberArrayEncryption(self, array):
        encryption = []
        for i in range(0, len(array)):
            encryption.append(self.numberEncryption(array[i]))
        return encryption

    # Encryption d'un texte en un array de nombres selon la position des caractères dans la table ASCII
    def textEncryption(self, text):
        encryption = []
        for character in text:
            encryption.append(self.numberEncryption(ord(character)))
        return encryption