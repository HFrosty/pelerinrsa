from mathtools import *
from RSADecryptor import *
import time

class RSAInterceptor:
    # On construit un intercepteur avec une clé publique
    def __init__(self, e, n):
        self.e = e
        self.n = n
        self.crackPAndQ()

    # A partir de la clé publique précédemment fournie, on retrouve p et q en parcourant tous les entiers p de 2 à e, si p est primaire et un multiple de n, alors q = n/p
    def crackPAndQ(self):
        print("Cracking (p, q)...")
        startTime = time.time()
        self.p = 0
        self.q = 0
        for p in range(2, self.e):
            if(isPrime(p) and self.n%p == 0):
                    self.p = p
                    self.q = int(self.n/p)
                    print("Cracked (p, q) = (" + str(self.p) + ", " + str(self.q) + ") in", time.time() - startTime, "seconds")
                    return
        print("ERROR: Unable to crack (p, q)")

    # On construit un décrypteur à partir de (p, q) et du e précédemment fourni, le décrypteur s'occupe alors de déchiffrer le nombre
    def numberDecryption(self, number):
        decryptor = RSADecryptor(self.p, self.q, self.e)
        return decryptor.numberDecryption(number)

    # On construit un décrypteur à partir de (p, q) et du e précédemment fourni, le décrypteur s'occupe alors de déchiffrer le tableau de nombre
    def numberArrayDecryption(self, array):
        decryptor = RSADecryptor(self.p, self.q, self.e)
        return decryptor.numberArrayDecryption(array)
        