import math

def gcd(a,b): 
    return math.gcd(a, b)

def isPrime(a):
    if(a == 1):
        return True
    else:
        for i in range(2, a):
            if(a % i == 0):
                return False
        return True