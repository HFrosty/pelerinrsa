from RSACryptor import *
from RSADecryptor import *
from RSAInterceptor import *

# 3 classes :
# RSACryptor permet de crypter un nombre à partir d'une clé publique
# RSADecryptor permet de décrypter un ou des nombres à partir d'un couple (p, q)
# RSAInterceptor peut décrypter un ou des nombres à partir d'une clé publique, pour cela, la classe RSAInterceptor se sert d'un objet RSADecryptor

def main():
    decryptor = RSADecryptor(1511, 907, 377)
    cryptor = RSACryptor(decryptor.e, decryptor.n)

    encryptedText = cryptor.textEncryption("Bonjour je suis Bob")
    print("Encrypted text: " + str(encryptedText))
    decryptedText = decryptor.textDecryption(encryptedText)
    print("Decrypted text: " + decryptedText)

main()
